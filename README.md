UV Box Timer project

To compile the code, copy the content of the directory Energia_libraries into the Energia directory for libraries.
Then open the sketch UVBoxTimer/UVBoxTimer.ino.

It is necessary to have these libraries installed in the library directory:

- LCD_screen
- LiquidCrystal
- SD

Schematics

The old schematic used to generate the PCB is under Eagle (old_material), UVBoxTimer_g, UVBoxSchematic_r
The new schematic is hosted on EasyEDA but is private.

Check the blog for more information :

- https://hanixdiy.blogspot.com/2015/11/timer-for-uv-box.html
- https://hanixdiy.blogspot.com/2018/12/digital-encoder-breakout.html
